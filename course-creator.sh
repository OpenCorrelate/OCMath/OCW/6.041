#! /bin/sh

create () {
    local root=$1
    local x=$2
    local y=$3

    for i in $(seq -f "%02g" $x $y)
    do
      mkdir -p ${root}${i}/{scratch,exercises}
      touch ${root}${i}/{README.md,exercises/README.md}
    done

}

create "texts/bertsekas_intro-to-probability/chapter" 1 7
create "notes/lec" 1 25
create "assignments/set" 1 11
create "exams/exam" 1 3
